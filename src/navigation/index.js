import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../screens/Home'
import SignUp from '../screens/signup'
import listfilm from '../screens/listfilm';
import Statment from'../screens/statment';
import crud from'../screens/crud';
import lifecycle from'../screens/lifecycle';
import splash from'../screens/splash';
import redux from '../screens/redux';
import tugas from '../screens/tugas ';
import tugasfibo from '../screens/tugasfibo';
import tugasgenap from '../screens/tugasgenap';
import tugasusers from '../screens/tugasusers';
import tugaspos from '../screens/tugaspos';
import tugasanastasia from '../screens/tugasanastasia';
import tugasprima from '../screens/tugasprima';
import latihan from '../screens/latihan';
import firebase from '../screens/firebase';
import firebasedata from '../screens/firebasedata';
import asynfirebase from '../screens/asynfirebase';
import inbox from '../screens/inbox';
import inboxdetail from '../screens/inboxdetail';
import remed from '../screens/remed';
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='splash'>
        <Stack.Screen options={{headerShown: false}} name='Home' component={Home} />
        <Stack.Screen options={{headerShown: false}} name='signup' component={SignUp} />
        <Stack.Screen name='listfilm' component={listfilm} />
        <Stack.Screen name='statment' component={Statment} />
        <Stack.Screen name='crud' component={crud} />
        <Stack.Screen name='lifecycle' component={lifecycle} />
        <Stack.Screen name='redux' component={redux} />
        <Stack.Screen name='tugas' component={tugas} />
        <Stack.Screen name='tugasfibo' component={tugasfibo} />
        <Stack.Screen name='tugasgenap' component={tugasgenap} />
        <Stack.Screen name='tugasusers' component={tugasusers} />
        <Stack.Screen name='tugaspos' component={tugaspos} />
        <Stack.Screen name='tugasanastasia' component={tugasanastasia} />
        <Stack.Screen name='tugasprima' component={tugasprima} />
        <Stack.Screen name='latihan' component={latihan} />
        <Stack.Screen name='firebase' component={firebase} />
        <Stack.Screen name='firebasedata' component={firebasedata} />
        <Stack.Screen name='asynfirebase' component={asynfirebase} />
        <Stack.Screen name='inbox' component={inbox} />
        <Stack.Screen name='inboxdetail' component={inboxdetail} />
        <Stack.Screen name='remed' component={remed} />
        <Stack.Screen options={{headerShown: false}} name='splash' component={splash} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;