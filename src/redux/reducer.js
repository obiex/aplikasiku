const initialstate = {
    test: 'AL Kocheng',
    students: [
        { id: 1, name: 'mahrus', addres: 'tanggerang' },
        { id: 2, name: 'robi', addres: 'tasik' },
        { id: 3, name: 'sueb', addres: 'jakarta' },
        { id: 4, name: 'maman', addres: 'jogja' },
    ],
    inbox: []
}
const reducer = (state = initialstate, action) => {
    console.log('aaaa', state, action)
    switch (action.type) {
        case 'ADD-STUDENTS':
            return {
                ...state,
                students: [...state.students, action.payload]
            };
        case 'DELETE-STUDENTS':
            return {
                ...state,
                students: state.students.filter(value => {
                    return value.id != action.payload
                })
            };
        case 'INBOX':
            return {
                ...state,
                inbox: [action.payload,...state.inbox]
            };
        case 'UPDATE-STUDENTS':
            return {
                ...state,
                students: action.payload
            };

        default:
            return state;
    }
};

export default reducer;