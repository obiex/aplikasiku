import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button, Alert, TouchableOpacity, Image } from 'react-native';
import axios from 'axios';
import listfilm from '../../screens/listfilm';
import CButton from '../../component/atom/CButton';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
export default class SignUp extends Component {

  constructor(params) {
    super(params);
    this.state = {
      data: [],
      username: '',
      password: ''


    };
  }

  componentDidMount() {
    axios.get('https://dummyjson.com/users')
      .then(result => this.setState({ data: result.data.users }))
  }

  _filter() {
    const { data } = this.state
    const hasil = data.filter(result => {
      return (
        result.username == this.state.username &&
        result.password == this.state.password
      )
    })
    console.log(hasil)
    hasil.length > 0 ? this.props.navigation.navigate('statment') : Alert.alert('username dan password salah')
  }

  render() {
    const { data } = this.state
    return (
      <View
        behavior="padding"
        style={styles.Wrapper}>
        <Image style={styles.test} source={require('../../assets/media/biowash.jpeg')} />
        <Text style={{ fontSize: 23, color: 'white' }}>LOGIN</Text>
        <View style={{ flexDirection: 'row' }}>
          <AntDesign name='user' size={20} style={{ color: 'white', marginTop: 20 }} />
          <TextInput
            placeholder='nama'
            underlineColorAndroid='white'
            placeholderTextColor='white'
            style={styles.inputField}
            onChangeText={value => this.setState({ username: value })}
          />

        </View>
        <View style={{ flexDirection: 'row' }}>
          <MaterialCommunityIcons name='onepassword' size={20} style={{ color: 'white', marginTop: 20 }} />
          <TextInput
            placeholder='password'
            underlineColorAndroid='white'
            placeholderTextColor='white'
            secureTextEntry={true}
            style={styles.inputField}
            onChangeText={value => this.setState({ password: value })}
          />
        </View>

        <View>
          <CButton title='Masuk' onPress={() => { this._filter() }} style={{ color: 'white', fontSize: 14, marginTop: 10 }} />
        </View>
        <TouchableOpacity>
          <Text style={{ color: 'white', marginTop: 10 }}>SignUp</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row' }}>
          <AntDesign name='facebook-square' size={30} style={{ color: 'white', marginTop: 20, marginEnd: 10 }} />
          <FontAwesome name='whatsapp' size={30} style={{ color: 'white', marginTop: 20, marginEnd: 10 }} />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  inputField: {
    width: 280,
    color: 'white',
    marginTop: 5
  },
  Wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1F3A93'
  },
  text: {
    color: 'blue',
    fontSize: 23
  },

  test: {
    justifyContent: 'center',
    paddingBottom: 100,
    width: 150,
    height: 50,
    resizeMode: 'stretch',


  },
});