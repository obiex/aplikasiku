import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

export default class index extends Component {
    constructor() {
      super();
      this.state = {
        array: [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
          20,
        ],
      };
    }
  
    render() {
      const {array} = this.state;
  return(
    
      <View>
          {array.map((x, i) =>{
        if (x > 1) {
          for (let y = 2; y < x; y++) {
            if (x % y == 0) {
              return;
            }
          }
          return (
            <View key={i}>
              <Text style={styles.text}>{x}</Text>
            </View>
          );
        }
      })}

      <View></View>
    </View>

  )
      
    }
  }
  

const styles = StyleSheet.create({
  text: {
    padding: 10,
    borderRadius: 20,
    borderwidth: 1,
    margin: 5,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
  
},
});

