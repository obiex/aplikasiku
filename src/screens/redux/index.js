import { Text, StyleSheet, View, TextInput, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import CButton from '../../component/atom/CButton';

export class index extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            name: '',
            addres: '',
            id: '',
            onselect: false,
            onEdit: false,
            checked: true,

        };
    }

    componentDidMount() { }
    _addData = () => {
        const { id, name, addres } = this.state;
        const data = { id, name, addres }
        this.props.add(data)
        this.setState(
            {
                id: '',
                name: '',
                addres: ''
            }
        )





    }

    // _addData = () => {
    //     const { name, addres, id } = this.state;
    //     const data = { name, addres, id };
    //     this.props.add(data);
    //     this.setState({
    //         name: '',
    //         addres: '',
    //         id: '',

    //     });
    // };
    _onEdit = value => {
        this.setState({
            onEdit: !this.state.onEdit,
            id: value.id,
            name: value.name,
            addres: value.addres,
        });
    };


    _delete = id => {
        this.props.delete(id);
    };
    _submitUpdate = () => {
        const { id, name, addres } = this.state;
        const { update, students } = this.props;
        const data = { id, name, addres };
        const updateStudents = students.map(value => {
            if (value.id === data.id) {
                (value.id = data.id),
                    (value.name = data.name),
                    (value.addres = data.addres);
                return value;
            } else {
                return value;
            }
        });
        console.log(updateStudents)
        this.props.update(updateStudents);
        this.setState({
            onEdit: false,
            name: '',
            addres: '',
        });
    };
    _cancel = () => {
        this.setState({
            name: '',
            id: '',
            addres: '',
            onEdit: false,
        });
    };




    render() {
        const { data, name, addres, id, onEdit, checked } = this.state;
        const { students } = this.props;
        const white = 'white';

        return (

            <View>
                <View>
                    <Text><Text>{this.props.test}</Text></Text>
                </View>
                <View style={{ justifyContent: 'space-between' }}>
                    {students.map((value, i) => {
                        return (<View key={i}>
                            <Text>{JSON.stringify(value)}</Text>
                            {/* <Text>{value.name}</Text>
                         <Text>{value}</Text> */}
                            <TouchableOpacity onPress={() => { this._delete(value.id) }} >
                                <Text > Delete</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this._onEdit(value) }} >
                                <Text > Update</Text>
                            </TouchableOpacity>
                        </View>

                        )
                    }

                    )}


                    {/* {students.map((value, index) => {
                        return <View key={index}
                            style={styles.card}>
                            <Text>{value.id} </Text>
                            <Text>{value.name} </Text>
                            <Text>{value.addres} </Text>
                            
                            <TouchableOpacity onPress={() => { this._delete(value.id) }} >
                                <Text > Delete</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this._onEdit(value) }} >
                                <Text > Update</Text>
                            </TouchableOpacity>
                        </View>
                    })} */}

                    <CButton title="ADD DATA" onPress={this._addData} />
                    <CButton title="On EDIT" onPress={this._onEdit} />
                </View>
                <View>
                    <TextInput value={this.state.id} placeholder='id' onChangeText={(text) => { this.setState({ id: text }) }} />
                    <TextInput value={this.state.name} placeholder='nama' onChangeText={(text) => { this.setState({ name: text }) }} />
                    <TextInput value={this.state.addres} placeholder='addres' onChangeText={(text) => { this.setState({ addres: text }) }} />
                </View>
                <CButton title="update data" onPress={this._submitUpdate} />
                <CButton title="cancel" onPress={this._cancel} />

            </View>
        );
    }
}
const mapstateToProps = (state) => {
    return {
        students: state.students,
        test: state.test,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        delete: (data) => {
            dispatch({
                type: 'DELETE-STUDENTS',
                payload: data
            })
        },
        add: (data) => {
            dispatch({
                type: 'ADD-STUDENTS',
                payload: data
            })
        },
        update: (data) => {
            dispatch({
                type: 'UPDATE-STUDENTS',
                payload: data
            })
        },

    };

};





export default connect(mapstateToProps, mapDispatchToProps)(index)
const styles = StyleSheet.create({
    card: {
        paddingTop: 10,
        flexDirection: 'row',
        borderRadius: 10,
        borderwidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'auto',


    },
    TextInput: {
        alignItems: 'center',
        marginTop: 15
    }

});
