import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default class index extends Component {
    constructor (){
       super();
       this.state ={
           data: [],
       }
    }
async componentDidMount(){
    this._saveData();
    this._getData();
}
_saveData = async () => {
    const stringValue = 'Test Al Kocheng';
    const objectValue = {
        name: 'obiex',
        addres: 'Depok',
    };
    const arrayValue = [1,2,3,4];

    const converToString = JSON.stringify(arrayValue);

    try {
        await AsyncStorage.setItem('TestStorage', converToString);
    } catch (e) {
        console.log(e);
    }
};
_getData = async () => {
    try {
        const jsonValue = await AsyncStorage.getItem('TestStorage');
        return(
            jsonValue != null && this.setState({data: JSON.parse(jsonValue)})
        
        );

    } catch (e) {
    }
}
    
  
  render() {
      const {data} = this.state;
      console.log(data);
    return (
      <View>
        <Text>Test AsyncStorage</Text>
        {data.length > 0 && data.map (v => {
            return <Text>{v}</Text>
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
