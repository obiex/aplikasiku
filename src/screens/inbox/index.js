import { Text, StyleSheet, View, Button, FlatList, Touchable, TouchableOpacity, ScrollView } from 'react-native'
import React, { Component } from 'react'
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { Store } from '../../redux/store';

 class index extends Component {
    constructor() {
        super()
        this.state = {
            secure: true,
            data: [],
        }
    }
    componentDidMount() {
  
        this._getData()
        messaging()
            .getToken()
            .then(fcmtoken => {
                console.log(fcmtoken)
            });
    }

    _getData = async () => {
        try {
            const jsonValue = await AsyncStorageLib.getItem('newInboxData');
            return (
                jsonValue != null && this.setState({ data: JSON.parse(jsonValue) })
            );
        } catch (e) {
            console.log(e)
        }
    }
    _removeItemValue = async () => {
        try {
            this.setState({ data: [] });
            await AsyncStorageLib.removeItem('newInboxData');
        } catch (e) {
            console.log(e)
        }
    }
    render() {
        const { inbox } = this.props;
        const data = inbox;
        console.log('abcde',inbox)
        return (
            <View style={{backgroundColor:'#1F3A93', flex:1,}}>
                <View style={{ marginRight: 320, paddingRight: 5, marginTop: 5, marginLeft: 5, alignItems:'center'}}>
                </View >
                <View style={{alignSelf:'center'}}>
                <TouchableOpacity style={styles.button} onPress={() => { this._removeItemValue() }} >
                    <Text style={{color:'white',alignItems:'center'}}> Delete</Text>
                </TouchableOpacity>
                </View>
                <ScrollView>
                    {data && data.map((v, i) => {
                        // console.log(v)
                        return (
                            <View  key={i} style={styles.textItemLogin}>
                                <Text style={{alignSelf:'center', fontSize:20, color:'white'}}>{v.body}</Text>
                                <Text style={{alignSelf:'center',fontSize:20, color:'white'}}>{v.title}</Text>
                                <TouchableOpacity style={styles.button} onPress={() => {
                                    this.props.navigation.navigate('inboxdetail', { message: { title: v.title, body: v.body } })
                                }}>
                                    <Text >Detail</Text>
                                </TouchableOpacity>

                            </View>
                        )
                    })}
                </ScrollView>
                <Text></Text>
            </View>
        )
    }
}
const mapStateToProp= (state) => {
    const {inbox} = state 
    return {inbox}
} 
export default connect(mapStateToProp,null )(index)
const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        marginTop: 65,
      },
      logo: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        alignSelf:'flex-start',
        marginLeft:35
      },
      header: {
        fontSize: 10,
        fontWeight: '100',
        color: 'black',
        alignSelf:'flex-start',
        marginLeft:35,
        borderWidth: 2 ,
        borderRadius: 2,
        borderColor:'white'
        
      },
      font: {
        color: 'black',
        fontSize:18,
        marginBottom: 30,
        width:350,
        alignSelf:'flex-start',
        marginLeft:35
      },
      input: {
        flexDirection: 'row',
        borderWidth: 2,
        borderRadius: 2,
        paddingLeft: 2,
        alignItems: 'center',
        marginVertical: 2,
        alignSelf:'center',
        backgroundColor:'purple'
      },
      textInput: {
        width: 290,
        fontSize: 21,
        paddingLeft: 20,
      },
      button: {
        flexDirection:'row',
        backgroundColor: '#FF0066',
        paddingVertical: 5,
        width: 330,
        height: 50,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
      },
      fontButton: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
      },
      font1: {
        justifyContent: 'center',
        marginTop:20
      },
      textItemLogin: {
        backgroundColor: '#29A391',
        padding: 20,
         marginVertical: 25,
         marginHorizontal: 25,
        textAlign: 'center',
        marginVertical: 8,
        alignSelf:'stretch',
        fontWeight: 'bold',
        color: 'white',
        fontFamily:'arial',
        borderRadius: 40,
        fontSize:100,   
        borderBottomWidth: 5,  
        },
})
