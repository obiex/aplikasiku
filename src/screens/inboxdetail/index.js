import { Text, StyleSheet, View } from 'react-native'
import React, { Component } from 'react'

export default class index extends Component {
    constructor(props){
        super(props)
        this.state = { 
        }
    }

    componentDidMount() {
    }
  render() {
      const {message} =this.props.route.params
    return (
      <View style={styles.textItemLogin}>
        <Text style={{alignSelf:'center',fontSize:20, color:'white'}}>{message.title}</Text>
        <Text style={{alignSelf:'center',fontSize:20, color:'white'}}>{message.body}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        marginTop: 65,
      },
      logo: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        alignSelf:'flex-start',
        marginLeft:35
      },
      header: {
        fontSize: 10,
        fontWeight: '100',
        color: 'black',
        alignSelf:'flex-start',
        marginLeft:35,
        borderWidth: 2 ,
        borderRadius: 2,
        borderColor:'black',
        backgroundColor:'blue'
        
        
      },
      font: {
        color: 'black',
        fontSize:18,
        marginBottom: 30,
        width:350,
        alignSelf:'flex-start',
        marginLeft:35
      },
      input: {
        flexDirection: 'row',
        borderWidth: 2,
        borderRadius: 2,
        paddingLeft: 2,
        alignItems: 'center',
        marginVertical: 2,
        alignSelf:'center',
        backgroundColor:'purple'
      },
      textInput: {
        width: 290,
        fontSize: 21,
        paddingLeft: 20,
      },
      button: {
        flexDirection:'row',
        backgroundColor: '#FF0066',
        paddingVertical: 5,
        width: 330,
        height: 50,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
      },
      fontButton: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
      },
      font1: {
        justifyContent: 'center',
        marginTop:20
      },
      textItemLogin: {
        backgroundColor: '#29A391',
        padding: 20,
         marginVertical: 25,
         marginHorizontal: 25,
        textAlign: 'center',
        marginVertical: 8,
        alignSelf:'stretch',
        fontWeight: 'bold',
        color: 'white',
        fontFamily:'arial',
        borderRadius: 40,
        fontSize:100,   
        borderBottomWidth: 5,  
        },
})