import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';
import firestore from '@react-native-firebase/firestore';

export default class index extends Component {
    constructor(){
        super ()
        this.state={
            data:[]
        }
    }
    componentDidMount(){
    firestore() 
    .collection('users')
    .doc('pelanggan')
    .onSnapshot(result => {
        this.setState({ data: result.data() })
    });
    let datapelanggan;
    firestore()
      .collection('users')
      .onSnapshot(value => {
       datapelanggan = value.docs.map(result => {
        return result.data();
      });
      datapelanggan.length >0 && this.setState({data: datapelanggan})
    })
    }
  render() {
      const {data} = this.state;
      console.log (data)

    return (
      <View style={{justifyContent:'center', alignItems:'center', flex:1}}> 
        <Text> Test Firebase data</Text>
        {data.length > 0 &&
        data.map((v, i)=> {
          return <Text key={i}>{v.nama}</Text>
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
