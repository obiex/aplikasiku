// import React ,{Component} from 'react';
// import { Alert } from 'react-native';
// import Navigation  from './src/navigation/index';
// import { Provider } from 'react-redux';
// import { PersistGate } from 'redux-persist/integration/react';
// import { Persistor, Store } from './src/redux/store';
// import messaging from '@react-native-firebase/messaging';



// export default class App extends Component {
//   componentDidMount(){
//     messaging().onMessage(async remoteMessage => {
//       Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
//     });
//   }
//   render (){
//     return (
//       <Provider store={Store}>
//         <PersistGate loading={null} persistor={Persistor}>
//           <Navigation/>
//         </PersistGate>
//       </Provider>
//         )
//   }
//  }
// import { Text, Alert, StyleSheet, View } from 'react-native';
// import React, { Component } from 'react';
// import messaging from '@react-native-firebase/messaging';
// import AsyncStorage from '@react-native-async-storage/async-storage'
// export default class App extends Component {
// componentDidMount() {
//     messaging().onMessage(async remoteMessage => {
//         Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
//       });
// }
// render() {
// return (
// <View>
// <Text>TEST COBA LAGI LAGI DAN LAGI </Text>
// </View>
// );
// }
// }
// const styles = StyleSheet.create({});

import React from 'react';
import Navigation from './src/navigation'
import { Persistor, Store } from './src/redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';

// foreground
const unsubscribe = messaging().onMessage(async remoteMessage => {
  console.log(remoteMessage)
  Store.dispatch({ type: 'INBOX', payload: {
    title: remoteMessage.notification.title,
    body: remoteMessage.notification.body,
    data: remoteMessage.data,
    isRead: 0
  } })
  setInboxData(remoteMessage)
});

// background
messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log(remoteMessage)
  Store.dispatch({
    type: 'INBOX', payload: {
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      data: remoteMessage.data,
      isRead: 0
    }
  })
  setInboxData(remoteMessage)
});

const setInboxData = async (remoteMessage) => {
  try {
    let getInboxData = await AsyncStorageLib.getItem('newInboxData');
    getInboxData = JSON.parse(getInboxData);

    if (!getInboxData) {
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(
          [
            {
              title: remoteMessage.notification.title,
              body: remoteMessage.notification.body,
              data: remoteMessage.data,
              isRead: 0
            }
          ]
        )
      )
    } else {
      const arrayData = [
        ...[
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            data: remoteMessage.data,
            isRead: 0
          }
        ],
        ...getInboxData
      ].slice(0, 20)
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(arrayData)
      )
    }
  } catch (error) {
    console.log(error);
  }
}

const App = () => {

  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
